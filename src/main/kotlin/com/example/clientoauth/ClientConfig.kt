package com.example.clientoauth

class ClientConfig {

    companion object {
        val baseUrl = "https://pacific-reef-41077.herokuapp.com/oauth/"
        val clientId = "2"
        val redirectUri = "https://cryptic-refuge-28342.herokuapp.com/auth"
        val clientSecret = "fc8fb46d-1a07-4f1a-92eb-9157c611a948"

        val authServerUrl = baseUrl + "authorize/" +
                "?client_id=" +
                ClientConfig.clientId +
                "&redirect_uri=" +
                ClientConfig.redirectUri
    }
}