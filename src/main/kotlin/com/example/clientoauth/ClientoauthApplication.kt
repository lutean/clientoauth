package com.example.clientoauth

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ClientoauthApplication

fun main(args: Array<String>) {
	runApplication<ClientoauthApplication>(*args)
}

