package com.example.clientoauth.containers


data class LoginResponse (val success: Boolean, val path: String)