package com.example.resttest.containers

data class UserResponse(var id: Long, var name: String, var email: String, var login: String)