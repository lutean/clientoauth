package com.example.clientoauth.controllers

import com.example.clientoauth.ClientConfig
import com.example.clientoauth.containers.LoginResponse
import okhttp3.*
import okhttp3.RequestBody
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.ModelAndView

@RestController
@RequestMapping("/api")
class ApiController {

    @GetMapping("/login")
    fun login() : LoginResponse{
        return LoginResponse(true, ClientConfig.authServerUrl)
    }

    @GetMapping("/auth")
    fun auth(@RequestParam("code") code: String): String  {

        var resp = getTokenAn(code) ?: ""

/*        val JSON = MediaType.parse("application/json; charset=utf-8")
        var body = ResponseBody.create(JSON, resp)*/

        return resp
    }

    fun getTokenAn(code: String): String? {

        val url = ClientConfig.baseUrl + "access_token"
        val JSON = MediaType.parse("application/json; charset=utf-8")
        var json = "{" +
                "\"client_id\":\"${ClientConfig.clientId}\"," +
                "\"client_secret\":\"${ClientConfig.clientSecret}\"," +
                "\"redirect_uri\":\"${ClientConfig.redirectUri}\"," +
                "\"code\":\"$code\"" +
                "}"

        val body = RequestBody.create(JSON, json)


/*
        val formBody = FormBody.Builder()
                .add("client_id", clientId)
                .add("client_secret", clientSecret)
//                .add("redirect_uri", redirectUrl)
                .add("code", code)
                .build()
*/


        var logging = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger {
            System.out.println(it)
        })
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)

        var client = OkHttpClient().newBuilder()
                .addInterceptor(logging)
                .build()


        val request = Request.Builder()
                .url(url)
                .post(body)
                .build()

        client.newCall(request).execute().use { response ->

            var str = response.body()?.string()
            return str
        }

    }


}