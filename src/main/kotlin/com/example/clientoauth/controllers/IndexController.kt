package com.example.clientoauth.controllers

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.servlet.ModelAndView


@Controller
class IndexController {


    @GetMapping("/")
    fun index(): ModelAndView {
        val model = HashMap<String, String>()
        model["title"] = "Anton"
        return ModelAndView("index", model)
    }

/*
    @GetMapping("/login")
    fun login(): ModelAndView {
        return ModelAndView("redirect:$authServerUrl")
    }
*/

/*
    @GetMapping("/auth")
    fun auth(@RequestParam("code") code: String): ModelAndView {

        var resp = getTokenAn(code)
        val model = HashMap<String, String>()
        if (resp != null)
            model["title"] = resp
        else
            model["title"] = ""
        return ModelAndView("index", model)
    }

*/



}